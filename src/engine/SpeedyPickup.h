#ifndef SPEEDYPICKUP_H_
#define SPEEDYPICKUP_H_

#include "GameObject.h"
#include "World.h"

class SpeedyPickup : public GameObject
{
public:
	CLASS_IDENTIFICATION( 'SPDP', GameObject)

		enum ESpeedyReplicationState {
		ECRS_Pose = 1 << 0,
		ECRS_Color = 1 << 1,
		ECRS_PickupId = 1 << 2,


		ECRS_AllState = ECRS_Pose | ECRS_Color | ECRS_PickupId
	};

	static GameObject* StaticCreate() { return new SpeedyPickup(); }
	static GameObjectPtr StaticCreatePtr() { return GameObjectPtr(new SpeedyPickup()); }

	virtual uint32_t GetAllStateMask() const override { return ECRS_AllState; }

	virtual void Update() override;



	void		SetPickupId(uint32_t inPickupId) { mPickupId = inPickupId; }
	uint32_t	GetPickupId()						const { return mPickupId; }

	bool operator==(SpeedyPickup &other);

	//	virtual void	Read( InputMemoryBitStream& inInputStream ) override;

	void Write(OutputMemoryBitStream& inOutputStream) const override;

	float GetLastMoveTimestamp() { return mLastMoveTimestamp; }
protected:
	SpeedyPickup();

private:
	uint32_t			mPickupId;

protected:
	float				mLastMoveTimestamp;

};

typedef shared_ptr< SpeedyPickup >	SpeedyPickupPtr;

#endif // SPEEDYPICKUP_H_