#include "SpeedyPickup.h"
#include "Maths.h"
#include "InputState.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

SpeedyPickup::SpeedyPickup() :
	GameObject(),
	mPickupId(0),
	mLastMoveTimestamp(0.0f)
{
	SetCollisionRadius(0.5f);
}

void SpeedyPickup::Update() {

}

void SpeedyPickup::Write(OutputMemoryBitStream& inOutputStream) const
{
	
		inOutputStream.Write(GetPickupId());

		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		inOutputStream.Write(GetRotation());
		inOutputStream.Write(GetColor());
		
	
}

bool SpeedyPickup::operator==(SpeedyPickup &other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if (!GameObject::operator==(other)) return false;
	if (this->ECRS_AllState != other.ECRS_AllState) return false;
	if (this->mPickupId != other.mPickupId) return false;
	if (!Maths::FP_EQUAL(this->mLastMoveTimestamp, other.mLastMoveTimestamp)) return false;

	return true;
}