#ifndef BULLET_H_
#define BULLET_H_

#include "GameObject.h"
#include "World.h"

//class InputState;

/* We'll later create client and server versions of this class */

class Bullet : public GameObject
{
public:
	CLASS_IDENTIFICATION('BULT', GameObject)

		enum EPlayerReplicationState
	{
		ECRS_Pose = 1 << 0,
		ECRS_Color = 1 << 1,
		ECRS_BulletId = 1 << 2,
		ECRS_Health = 1 << 3,

		ECRS_AllState = ECRS_Pose | ECRS_Color | ECRS_BulletId | ECRS_Health
	};

	static	GameObject*	StaticCreate() { return new Bullet(); }

	//Note - the code in the book doesn't provide this until the client.
	//This however limits testing.
	static	GameObjectPtr	StaticCreatePtr() { return GameObjectPtr(new Bullet()); }


	virtual uint32_t GetAllStateMask()	const override { return ECRS_AllState; }

	virtual void Update() override;

	void ProcessCollisions();
	void ProcessCollisionsWithScreenWalls();

	void		SetPlayerWhoShot(uint32_t inBulletId) { mPlayerWhoShotId = inBulletId; }
	uint32_t	GetPlayerWhoShot()						const { return mPlayerWhoShotId; }

	void		SetTeamIgnore(int inTeamNum) { mIgnoreTeamNum = inTeamNum; }

	void			SetVelocity(const Vector3& inVelocity) { mVelocity = inVelocity; }
	const Vector3&	GetVelocity()						const { return mVelocity; }
	void Write(OutputMemoryBitStream& inOutputStream) const override;

	// For testing

	float GetMaxLinearSpeed() { return mMaxLinearSpeed; }
	float GetMaxRotationSpeed() { return mMaxRotationSpeed; }
	float GetWallRestitution() { return mWallRestitution; }
	float GetNPCRestitution() { return mNPCRestitution; }
	float GetLastMoveTimestamp() { return mLastMoveTimestamp; }
	float GetThrustDir() { return mThrustDir; }

	bool operator==(Bullet &other);
protected:
	Bullet();

private:


	void	AdjustVelocityByThrust(float inDeltaTime);

	Vector3				mVelocity;


	float				mMaxLinearSpeed;
	float				mMaxRotationSpeed;

	//bounce fraction when hitting various things
	float				mWallRestitution;
	float				mNPCRestitution;


	uint32_t			mPlayerWhoShotId;

	int					mIgnoreTeamNum;

protected:

	///move down here for padding reasons...

	float				mLastMoveTimestamp;

	float				mThrustDir;


};

typedef shared_ptr< Bullet >	BulletPtr;

#endif // BULLET_H_
