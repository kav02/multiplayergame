#include "Block.h";
#include "Colors.h";
Block::Block() :
	GameObject()
{
	SetCollisionRadius(0.5f);
	SetColor(Colors::White);
}

bool Block::operator==(Block &other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if (!GameObject::operator==(other)) return false;
	return true;
}