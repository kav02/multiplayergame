#ifndef BLOCK_H_
#define BLOCK_H_

#include "GameObject.h"

class Block : public GameObject
{
public:
	CLASS_IDENTIFICATION('BLOK', GameObject)
	static GameObject* StaticCreate() { return new Block(); }
	static GameObjectPtr StaticCreatePtr() { return GameObjectPtr(new Block()); }

	bool operator==(Block &other);
protected:
	Block();
};

typedef shared_ptr<Block> BlockPtr;
#endif //BLOCK_H_