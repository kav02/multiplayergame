#include "Bullet.h"
#include "Timing.h"
#include "Maths.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Bullet::Bullet() :
	GameObject(),
	mVelocity(Vector3(0, 3, 0)),
	mMaxLinearSpeed(50.f),
	mMaxRotationSpeed(5.f),
	mWallRestitution(0.1f),
	mPlayerWhoShotId(0),
	mNPCRestitution(0.1f),
	mLastMoveTimestamp(0.0f),
	mThrustDir(3.0f)

{
	SetCollisionRadius(0.5f);
}

void Bullet::Write(OutputMemoryBitStream& inOutputStream) const
{

	inOutputStream.Write(GetPlayerWhoShot());

	Vector3 velocity = mVelocity;
	inOutputStream.Write(velocity.mX);
	inOutputStream.Write(velocity.mY);

	Vector3 location = GetLocation();
	inOutputStream.Write(location.mX);
	inOutputStream.Write(location.mY);

	inOutputStream.Write(GetRotation());
	inOutputStream.Write(mThrustDir > 0.f);
	inOutputStream.Write(GetColor());
}

void Bullet::AdjustVelocityByThrust(float inDeltaTime)
{
	//just set the velocity based on the thrust direction -- no thrust will lead to 0 velocity
	//simulating acceleration makes the client prediction a bit more complex
	Vector3 forwardVector = GetForwardVector();
	mVelocity = forwardVector * (mThrustDir * inDeltaTime * mMaxLinearSpeed);
}

void Bullet::Update()
{
	float deltaTime = Timing::sInstance.GetDeltaTime();
	AdjustVelocityByThrust(deltaTime);
	SetLocation(GetLocation() + mVelocity * deltaTime);
	ProcessCollisions();
}


void Bullet::ProcessCollisions() 
{
	//right now just bounce off the sides..
	ProcessCollisionsWithScreenWalls();

	float sourceRadius = GetCollisionRadius();
	Vector3 sourceLocation = GetLocation();

	//now let's iterate through the world and see what we hit...
	//note: since there's a small number of objects in our game, this is fine.
	//but in a real game, brute-force checking collisions against every other object is not efficient.
	//it would be preferable to use a quad tree or some other structure to minimize the
	//number of collisions that need to be tested.
	for (auto goIt = World::sInstance->GetGameObjects().begin(), end = World::sInstance->GetGameObjects().end(); goIt != end; ++goIt)
	{
		GameObject* target = goIt->get();

		if (target != this && !target->DoesWantToDie() )
		{

			if (mIgnoreTeamNum != target->GetTeamNum()) {
				//simple collision test for spheres- are the radii summed less than the distance?
				Vector3 targetLocation = target->GetLocation();
				float targetRadius = target->GetCollisionRadius();

				Vector3 delta = targetLocation - sourceLocation;
				float distSq = delta.LengthSq2D();
				float collisionDist = (sourceRadius + targetRadius);
				if (distSq < (collisionDist * collisionDist))
				{
					//first, tell the other guy there was a collision with a player, so it can do something...

					if (target->HandleCollisionWithBullet(this))
					{
						SetDoesWantToDie(true);
						HandleDying();
					}

				}
			}
		}
	}
}

void Bullet::ProcessCollisionsWithScreenWalls() //deletes the bullet if colliding with a wall of the world.
{
	Vector3 location = GetLocation();
	float x = location.mX;
	float y = location.mY;

	float vx = mVelocity.mX;
	float vy = mVelocity.mY;

	float radius = GetCollisionRadius();

	//if the cat collides against a wall, the quick solution is to push it off
	if ((y + radius) >= HALF_WORLD_HEIGHT && vy > 0)
	{
		SetDoesWantToDie(true);
		HandleDying();
	}
	else if (y <= (-HALF_WORLD_HEIGHT - radius) && vy < 0)
	{
		SetDoesWantToDie(true);
		HandleDying();
	}

	if ((x + radius) >= HALF_WORLD_WIDTH && vx > 0)
	{
		SetDoesWantToDie(true);
		HandleDying();
	}
	else if (x <= (-HALF_WORLD_WIDTH - radius) && vx < 0)
	{
		SetDoesWantToDie(true);
		HandleDying();
	}
}

bool Bullet::operator==(Bullet &other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if (!GameObject::operator==(other)) return false;

	if (this->ECRS_AllState != other.ECRS_AllState) return false;

	if (!Maths::Is3DVectorEqual(this->mVelocity, other.mVelocity)) return false;
	if (!Maths::FP_EQUAL(this->mMaxLinearSpeed, other.mMaxLinearSpeed)) return false;
	if (!Maths::FP_EQUAL(this->mMaxRotationSpeed, other.mMaxRotationSpeed)) return false;
	if (!Maths::FP_EQUAL(this->mWallRestitution, other.mWallRestitution)) return false;
	if (!Maths::FP_EQUAL(this->mNPCRestitution, other.mNPCRestitution)) return false;
	if (this->mPlayerWhoShotId != other.mPlayerWhoShotId) return false;

	if (!Maths::FP_EQUAL(this->mLastMoveTimestamp, other.mLastMoveTimestamp)) return false;
	if (!Maths::FP_EQUAL(this->mThrustDir, other.mThrustDir)) return false;

	return true;
}
