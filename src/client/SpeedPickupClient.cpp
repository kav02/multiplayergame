#include "SpeedPickupClient.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

#include "StringUtils.h"

SpeedPickupClient::SpeedPickupClient() :
	mTimeLocationBecameOutOfSync(0.f)

{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("spickup"));

}

void SpeedPickupClient::HandleDying() {
	SpeedyPickup::HandleDying();
}

void SpeedPickupClient::Update() {
	//nothing
}

void SpeedPickupClient::Read(InputMemoryBitStream& inInputStream) {
	uint32_t SpeedyPickupId;
	inInputStream.Read(SpeedyPickupId);
	SetPickupId(SpeedyPickupId);

	float oldRotation = GetRotation();
	Vector3 oldLocation = GetLocation();

	float replicatedRotation;
	Vector3 replicatedLocation;

	inInputStream.Read(replicatedLocation.mX);
	inInputStream.Read(replicatedLocation.mY);

	SetLocation(replicatedLocation);

	inInputStream.Read(replicatedRotation);
	SetRotation(replicatedRotation);

	Vector3 color;
	inInputStream.Read(color);
	SetColor(color);
}
