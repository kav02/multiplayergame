#include "BlockRender.h"

#include "TextureManager.h"
#include "StringUtils.h"

BlockRender::BlockRender()
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("block")); //set texture of block.
}



void BlockRender::HandleDying()
{
	Block::HandleDying();
}