#ifndef SPEEDPICKUP_CLIENT_H
#define SPEEDPICKUP_CLIENT_H

#include "SpeedyPickup.h"
#include "SpriteComponent.h"

class SpeedPickupClient : public SpeedyPickup
{
public:
	static GameObjectPtr StaticCreate() { return GameObjectPtr(new SpeedPickupClient()); }
	virtual void Update();
	virtual void HandleDying() override;
	virtual void Read(InputMemoryBitStream & inInputStream) override;

protected:
	SpeedPickupClient();

private:
		float	mTimeLocationBecameOutOfSync;
		
		SpriteComponentPtr mSpriteComponent;

};
typedef shared_ptr<SpeedPickupClient> SpeedPickupClientPtr;
#endif //SPEEDPICKUP_CLIENT_H