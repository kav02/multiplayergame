#ifndef BLOCK_RENDER_H
#define BLOCK_RENDER_H

#include "Block.h"
#include "SpriteComponent.h"

class BlockRender : public Block
{
public:
	static GameObjectPtr StaticCreate() { return GameObjectPtr(new BlockRender()); }
	virtual void HandleDying() override;
protected:
	BlockRender();
private:
	SpriteComponentPtr mSpriteComponent;

	int mObjectNum;
};
typedef shared_ptr< BlockRender > BlockRenderPtr;
#endif //BLOCK_RENDER_H