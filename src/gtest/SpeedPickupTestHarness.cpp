#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "World.h"
#include "GameObjectRegistry.h"
#include "SpeedPickupTestHarness.h"
#include "SpeedyPickup.h"
#include "SpeedPickupClient.h"

#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

SpeedPickupTestHarness::SpeedPickupTestHarness()
{
	sp = nullptr;
}

SpeedPickupTestHarness::~SpeedPickupTestHarness()
{
	sp.reset();
}

void SpeedPickupTestHarness::SetUp()
{
	GameObject*	go = SpeedyPickup::StaticCreate();
	SpeedyPickup* p = static_cast<SpeedyPickup*>(go);
	this->sp.reset(p);
}

void SpeedPickupTestHarness::TearDown()
{
	this->sp.reset();
	this->sp = nullptr;
}

TEST_F(SpeedPickupTestHarness, constructor_noArgs)
{
	// Check defaults are set
	// Should be no need to do these as they were tested with the base class.
	EXPECT_TRUE(Maths::Is3DVectorEqual(sp->GetColor(), Colors::White));
	EXPECT_TRUE(Maths::Is3DVectorEqual(sp->GetLocation(), Vector3::Zero));
	EXPECT_FLOAT_EQ(sp->GetCollisionRadius(), 0.5f);
	EXPECT_FLOAT_EQ(sp->GetScale(), 1.0f);
	EXPECT_FLOAT_EQ(sp->GetRotation(), 0.0f);
	EXPECT_EQ(sp->GetIndexInWorld(), -1);
	EXPECT_EQ(sp->GetNetworkId(), 0);

	//EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetVelocity(), Vector3(0, 3, 0)));


	//Check our macro has worked.
	
	EXPECT_EQ(sp->GetClassId(), 'SPDP');
	EXPECT_NE(sp->GetClassId(), 'HELP');

	//Added some getters so I could check these - not an easy class to test.

	EXPECT_FLOAT_EQ(sp->GetLastMoveTimestamp(), 0.0f);
	
}

TEST_F(SpeedPickupTestHarness, EqualsOperator1)
{
	SpeedyPickup *a = static_cast<SpeedyPickup*>(SpeedyPickup::StaticCreate());
	SpeedyPickup *b = static_cast<SpeedyPickup*>(SpeedyPickup::StaticCreate());

	a->SetPickupId(30);
	b->SetPickupId(10);

	EXPECT_GT(a->GetPickupId(), b->GetPickupId());

	EXPECT_FALSE(*a == *b);
}

TEST_F(SpeedPickupTestHarness, EqualsOperator2)
{
	SpeedyPickup *a = static_cast<SpeedyPickup*>(SpeedyPickup::StaticCreate());
	SpeedyPickup *b = static_cast<SpeedyPickup*>(SpeedyPickup::StaticCreate());

	a->SetPickupId(10);
	b->SetPickupId(10);

	EXPECT_TRUE(*a == *b);
}


TEST_F(SpeedPickupTestHarness, CanRegisterObject) //Testing to see if the game object registry will register as a game object.
{

	GameObjectRegistry::StaticInit();
	World::StaticInit();
	GameObjectRegistry::sInstance->RegisterCreationFunction('SPDP', &SpeedyPickup::StaticCreatePtr);


	EXPECT_NO_THROW(GameObjectRegistry::sInstance->CreateGameObject('SPDP'));

}