#ifndef BLOCK_TESTHARNESS_H
#define BLOCK_TESTHARNESS_H

#include <limits.h>
#include <gtest/gtest.h>

#include "Block.h"

class BlockTestHarness : public ::testing::Test
{
protected:

	virtual void SetUp();
	virtual void TearDown();

	BlockPtr bp;

public:

	BlockTestHarness();
	virtual ~BlockTestHarness();
};

#endif // BLOCK_TESTHARNESS_H
