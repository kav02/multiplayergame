#ifndef SPEEDPICKUP_TESTHARNESS_H
#define SPEEDPICKUP_TESTHARNESS_H

#include <limits.h>
#include <gtest/gtest.h>

#include "SpeedyPickup.h"

class SpeedPickupTestHarness : public ::testing::Test
{
protected:

	virtual void SetUp();
	virtual void TearDown();

	SpeedyPickupPtr sp;

public:

	SpeedPickupTestHarness();
	virtual ~SpeedPickupTestHarness();
};

#endif // SPEEDPICKUP_TESTHARNESS_H
