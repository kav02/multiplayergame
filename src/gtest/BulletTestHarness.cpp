#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "BulletTestHarness.h"
#include "GameObjectRegistry.h"
#include "Bullet.h"
#include "Bulletclient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"
#include "World.h"



#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

BulletTestHarness::BulletTestHarness() {
	bp = nullptr;
}

BulletTestHarness::~BulletTestHarness() {
	bp.reset();
}

void BulletTestHarness::SetUp() {
	GameObject* go = Bullet::StaticCreate();
	Bullet* b = static_cast<Bullet*>(go);
	this->bp.reset(b);
}

void BulletTestHarness::TearDown() {
	this->bp.reset();
	this->bp = nullptr;
}

TEST_F(BulletTestHarness, constructor_noArgs)
{
	// Check defaults are set
	// Should be no need to do these as they were tested with the base class.
	EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetColor(), Colors::White));
	EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetLocation(), Vector3::Zero));
	EXPECT_FLOAT_EQ(bp->GetCollisionRadius(), 0.5f);
	EXPECT_FLOAT_EQ(bp->GetScale(), 1.0f);
	EXPECT_FLOAT_EQ(bp->GetRotation(), 0.0f);
	EXPECT_EQ(bp->GetIndexInWorld(), -1);
	EXPECT_EQ(bp->GetNetworkId(), 0);

	//EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetVelocity(), Vector3(0, 3, 0)));

	//Initial state is update all
	int check = 0x000F; //Hex - binary 00000000 00000000 00000000 00001111
	EXPECT_EQ(bp->GetAllStateMask(), check);

	//Check our macro has worked.
	EXPECT_EQ(bp->GetClassId(), 'BULT');
	EXPECT_NE(bp->GetClassId(), 'HELP');

	//Added some getters so I could check these - not an easy class to test.
	EXPECT_FLOAT_EQ(bp->GetMaxLinearSpeed(), 50.0f);
	EXPECT_FLOAT_EQ(bp->GetMaxRotationSpeed(), 5.0f);
	EXPECT_FLOAT_EQ(bp->GetWallRestitution(), 0.1f);
	EXPECT_FLOAT_EQ(bp->GetNPCRestitution(), 0.1f);
	EXPECT_FLOAT_EQ(bp->GetLastMoveTimestamp(), 0.0f);
	EXPECT_FLOAT_EQ(bp->GetThrustDir(), 3.0f);
}

TEST_F(BulletTestHarness, EqualsOperator2)
{
	Bullet *a = static_cast<Bullet*>(Bullet::StaticCreate());
	Bullet *b = static_cast<Bullet*>(Bullet::StaticCreate());

	a->SetPlayerWhoShot(10);
	b->SetPlayerWhoShot(10);

	EXPECT_TRUE(*a == *b);
}

TEST_F(BulletTestHarness, EqualsOperator3)
{
	Bullet *a = static_cast<Bullet*>(Bullet::StaticCreate());
	Bullet *b = static_cast<Bullet*>(Bullet::StaticCreate());

	a->SetPlayerWhoShot(10);
	b->SetPlayerWhoShot(30);

	EXPECT_GT(b->GetPlayerWhoShot(), a->GetPlayerWhoShot());
}

TEST_F(BulletTestHarness, EqualsOperator4) //does 'a' equal itself?!
{
	Bullet *a = static_cast<Bullet*>(Bullet::StaticCreate());

	a->SetPlayerWhoShot(10);

	EXPECT_TRUE(*a == *a);
}

TEST_F(BulletTestHarness, EqualsOperator5) //Testing Colors
{
	Bullet *a = static_cast<Bullet*>(Bullet::StaticCreate());
	Bullet *b = static_cast<Bullet*>(Bullet::StaticCreate());

	b->SetPlayerWhoShot(10);
	a->SetPlayerWhoShot(10);
	a->SetColor(Colors::LightPink);
	b->SetColor(Colors::LightBlue);

	EXPECT_FALSE(*a == *b);

	b->SetColor(Colors::LightPink); //test their colours are now equal!

	EXPECT_TRUE(*a == *b);

}


TEST_F(BulletTestHarness, CanRegisterObject) //Testing to see if the game object registry will register as a game object.
{

	GameObjectRegistry::StaticInit();
	World::StaticInit();
	GameObjectRegistry::sInstance->RegisterCreationFunction('BULT', &Bullet::StaticCreatePtr);


	EXPECT_NO_THROW(GameObjectRegistry::sInstance->CreateGameObject('BULT'));

}

TEST_F(BulletTestHarness, serialiseAndDeserialiseBullet)
{
	BulletPtr testInput(static_cast<Bullet*>(Bullet::StaticCreate()));
	BulletClientPtr testOutput(static_cast<BulletClient*>(BulletClient::StaticCreate().get()));

	shared_ptr<InputMemoryBitStream> in;
	shared_ptr<OutputMemoryBitStream> out;

	const int BUFF_MAX = 1500;
	char *bigBuffer = new char[BUFF_MAX]; //testing only - gets replaced.
	in.reset(new InputMemoryBitStream(bigBuffer, BUFF_MAX));
	out.reset(new OutputMemoryBitStream());

	EXPECT_TRUE(*testInput == *testOutput); //expect constructed objs to be the same.

	// change this one a bit so I know the changes have copied over.
	testOutput->SetPlayerWhoShot(20);


	EXPECT_FALSE(*testInput == *testOutput); //with one changed should be different.

	//write it into a buffer.
	testOutput->Write(*out);

	// ... imagine networking goes on and we get an
  // actually we're connecting the output buffer to the input.
  // copy the buffer first (or we get double de-allocation)
	int copyLen = out->GetByteLength();
	char* copyBuff = new char[copyLen];
	memcpy(copyBuff, out->GetBufferPtr(), copyLen);

	in.reset(new InputMemoryBitStream(copyBuff, copyLen));

	// update from our server.
	testInput->Read(*in);

	// expect these to now be the same.
	EXPECT_TRUE(*testInput == *testOutput); //expect constructed objs to be the same. 
} 