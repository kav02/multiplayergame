
#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "GameObjectRegistry.h"
#include "World.h"
#include "BlockTestHarness.h"
#include "Block.h"
#include "BlockRender.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

BlockTestHarness::BlockTestHarness()
{
	bp = nullptr;
}

BlockTestHarness::~BlockTestHarness()
{
	bp.reset();
}

void BlockTestHarness::SetUp()
{
	GameObject*	go = Block::StaticCreate();
	Block* p = static_cast<Block*>(go);
	this->bp.reset(p);
}

void BlockTestHarness::TearDown()
{
	this->bp.reset();
	this->bp = nullptr;
}

TEST_F(BlockTestHarness, constructor_noArgs)
{
	// Check defaults are set
	// Should be no need to do these as they were tested with the base class.
	EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetColor(), Colors::White));
	EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetLocation(), Vector3::Zero));
	EXPECT_FLOAT_EQ(bp->GetCollisionRadius(), 0.5f);
	EXPECT_FLOAT_EQ(bp->GetScale(), 1.0f);
	EXPECT_FLOAT_EQ(bp->GetRotation(), 0.0f);
	EXPECT_EQ(bp->GetIndexInWorld(), -1);
	EXPECT_EQ(bp->GetNetworkId(), 0);

	//EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetVelocity(), Vector3(0, 3, 0)));

	//Check our macro has worked.
	EXPECT_EQ(bp->GetClassId(), 'BLOK');
	EXPECT_NE(bp->GetClassId(), 'HELP');
}

TEST_F(BlockTestHarness, TestClassName)
{
	Block *a = static_cast<Block*>(Block::StaticCreate());

	EXPECT_TRUE(a->GetClassId() == 'BLOK');
}

TEST_F(BlockTestHarness, EqualsOperation)
{
	Block *a = static_cast<Block*>(Block::StaticCreate());
	Block *b = static_cast<Block*>(Block::StaticCreate());

	EXPECT_TRUE(*a == *b);
}

TEST_F(BlockTestHarness, CanRegisterObject) //Testing to see if the game object registry will register as a game object.
{

	GameObjectRegistry::StaticInit();
	World::StaticInit();
	GameObjectRegistry::sInstance->RegisterCreationFunction('BLOK', &Block::StaticCreatePtr);


	EXPECT_NO_THROW(GameObjectRegistry::sInstance->CreateGameObject('BLOK'));

}
