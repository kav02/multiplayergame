#include "PlayerServer.h"
#include "BulletServer.h"
#include "GameObjectRegistry.h"
#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"

PlayerServer::PlayerServer() :
	mPlayerControlType( ESCT_Human ),
	mTimeOfNextShot( 0.f ),
	mTimeBetweenShots( 1.0f )
{}

void PlayerServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

void PlayerServer::Update()
{
	Player::Update();


	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();
	float oldRotation = GetRotation();

	ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy( GetPlayerId() );
	if( client )
	{
		MoveList& moveList = client->GetUnprocessedMoveList();
		for( const Move& unprocessedMove : moveList )
		{
			const InputState& currentState = unprocessedMove.GetInputState();
			float deltaTime = unprocessedMove.GetDeltaTime();
			ProcessInput( deltaTime, currentState );
			SimulateMovement( deltaTime );
		}

		moveList.Clear();
	}

	HandleShooting(GetPlayerId());
}

void PlayerServer::HandleShooting(uint32_t PlayerWhoShot)
{
	GameObjectRegistry::sInstance->RegisterCreationFunction('BULT', BulletServer::StaticCreate);
	float time = Timing::sInstance.GetFrameStartTime();
	if( mIsShooting && Timing::sInstance.GetFrameStartTime() > mTimeOfNextShot )
	{
		//not exact, but okay
		mTimeOfNextShot = time + mTimeBetweenShots;

		//fire!
		
		BulletPtr bullet = std::static_pointer_cast< Bullet > (GameObjectRegistry::sInstance->CreateGameObject('BULT'));
		bullet->SetPlayerWhoShot(PlayerWhoShot);
		//gotta pick a better spawn location than this...
		//bullet->SetLocation(Vector3(1.f - static_cast<float>(inBulletId), 0.f, 0.f));
		bullet->SetPhysicalCollide(false);
		bullet->SetLocation(Player::GetLocation());
		bullet->SetRotation(Player::GetRotation());
		bullet->SetTeamIgnore(Player::GetTeamNum());


	}
}

void PlayerServer::TakeDamage( int inDamagingPlayerId )
{
	mHealth--;
	if( mHealth <= 0.f )
	{
		//and you want to die
		SetDoesWantToDie( true );

		//tell the client proxy to make you a new cat
		ClientProxyPtr clientProxy = NetworkManagerServer::sInstance->GetClientProxy( GetPlayerId() );
		if( clientProxy )
		{
			clientProxy->HandlePlayerDied();
		}
	}
}
