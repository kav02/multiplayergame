#ifndef BLOCK_SERVER_H
#define BLOCK_SERVER_H

#include "Block.h"
#include "NetworkManagerServer.h"

class BlockServer : public Block
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BlockServer()); }
	virtual void HandleDying() override;
	
protected:
	BlockServer();
};
#endif