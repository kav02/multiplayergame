#ifndef SPEEDPICKUP_SERVER_H
#define SPEEDPICKUP_SERVER_H

#include "SpeedyPickup.h"
#include "NetworkManagerServer.h"

class SpeedPickupServer : public SpeedyPickup
{
public:
	static GameObjectPtr StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new SpeedPickupServer());}
	virtual void HandleDying() override;
	virtual void Update() override;

protected:
	SpeedPickupServer();
private:

};
#endif // SPEEDPICKUP_SERVER_H