#include "SpeedPickupServer.h"
#include "GameObjectRegistry.h"
#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"

SpeedPickupServer::SpeedPickupServer() {

}

void SpeedPickupServer::HandleDying()
{
	this->SetLocation(Vector3(0.f,100.f,0.f)); //used to check if the pickup exists anymore in server.
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void SpeedPickupServer::Update()
{
	SpeedyPickup::Update();


	Vector3 oldLocation = GetLocation();

	float oldRotation = GetRotation();

	ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy(GetPickupId());
	if (client)
	{
		MoveList& moveList = client->GetUnprocessedMoveList();
		for (const Move& unprocessedMove : moveList)
		{
			const InputState& currentState = unprocessedMove.GetInputState();
			float deltaTime = unprocessedMove.GetDeltaTime();
			//SimulateMovement(deltaTime);
		}

		moveList.Clear();
	}
}