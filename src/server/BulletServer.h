#ifndef BULLET_SERVER_H
#define BULLET_SERVER_H

#include "Bullet.h"
#include "NetworkManagerServer.h"
#include "PlayerServer.h"

class BulletServer : public Bullet
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BulletServer()); }
	virtual void HandleDying() override; 
	void SetBulletControlType(EPlayerControlType inBulletControlType) { mBulletControlType = inBulletControlType; }

	virtual void Update() override;

protected:
	BulletServer();

private:

	EPlayerControlType mBulletControlType;
};
#endif