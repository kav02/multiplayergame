
#include "Server.h"
#include "GameObjectRegistry.h"
#include "StringUtils.h"
#include "Colors.h"
#include "PlayerServer.h"
#include "BulletServer.h"
#include "BlockServer.h"
#include "SpeedPickupServer.h"
#include "Maths.h"


bool Server::StaticInit()
{
	sInstance.reset( new Server() );

	return true;
}

Server::Server() :
mTimeBetweenPickupChecks(15.f)
{
	pushColour(Colors::White); //initialises team colours into stack.
	pushColour(Colors::LightPink);

	pushColour(Colors::Green);
	pushColour(Colors::Black);

	pushColour(Colors::Red);
	pushColour(Colors::Blue);

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'PLYR', PlayerServer::StaticCreate );
//	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'BULT', BulletServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction('BLOK', BlockServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('SPDP', SpeedPickupServer::StaticCreate);

	InitNetworkManager();

	// Setup latency
	float latency = 0.0f;
	string latencyString = StringUtils::GetCommandLineArg( 2 );
	if( !latencyString.empty() )
	{
		latency = stof( latencyString );
	}
	NetworkManagerServer::sInstance->SetSimulatedLatency( latency );
}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	string portString = StringUtils::GetCommandLineArg( 1 );
	uint16_t port = stoi( portString );

	return NetworkManagerServer::StaticInit( port );
}


void Server::SetupWorld() //for static and non-changing game objects.
{
	
	BlockPtr block = std::static_pointer_cast<Block>(GameObjectRegistry::sInstance->CreateGameObject('BLOK'));
	block->SetLocation(Vector3(0.0f, 0.0f, 0.0f));
	
	
	//SpeedyPickupPtr Pickup = std::static_pointer_cast<SpeedyPickup>(GameObjectRegistry::sInstance->CreateGameObject('SPDP'));
	//Pickup->SetColor(Colors::Red);
	//Pickup->SetLocation(Vector3(0.0f, 3.0f, 0.0f));
	//Pickup->SetScale(0.5f);
	
}

void Server::SpawnSpeedPickup()  //creates the pickup.
{
	float time = Timing::sInstance.GetFrameStartTime();
	mTimeOfNextSpawn = time + mTimeBetweenPickupChecks;

	Pickup = std::static_pointer_cast<SpeedyPickup>(GameObjectRegistry::sInstance->CreateGameObject('SPDP'));
	Pickup->SetColor(Colors::Red);


	float Minx = -6.4f;
	float Maxx = 6.4f;

	float Miny = -3.6f;
	float Maxy = 3.6f;

	float rx = ((float(rand()) / float(RAND_MAX)) * (Maxx - Minx)) + Minx;
	float ry = ((float(rand()) / float(RAND_MAX)) * (Maxy - Miny)) + Miny;

	Pickup->SetLocation(Vector3(rx, ry, 0.0f));
	Pickup->SetScale(0.5f);
}

void Server::SpawnPickupPoll() //spawns pickups every 15 seconds randomly across the playing field!
{
	

	if (!Pickup) //if the pickup doesn't exist at the start of the game.
	{
		SpawnSpeedPickup();

	}else if (Timing::sInstance.GetFrameStartTime() > mTimeOfNextSpawn && Maths::Is3DVectorEqual(Pickup->GetLocation(), Vector3(0.f, 100.f, 0.f))) 
	{
		SpawnSpeedPickup();
	}
}

void Server::DoFrame()
{

	SpawnPickupPoll();

	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnPlayers();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();
}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{

	int playerId = inClientProxy->GetPlayerId();

	//int bulletId = inClientProxy->GetBulletId();
	//ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName() );
	SpawnPlayer( playerId );
	AddPlayerNum();
	//if (bulletId != 0) {
	//	SpawnBullet(bulletId);
	//}
	
}

void Server::SpawnPlayer( int inPlayerId ) //separates players into teams
{
	PlayerPtr player = std::static_pointer_cast< Player >( GameObjectRegistry::sInstance->CreateGameObject( 'PLYR' ) );
	//player->SetColor( popColour() );//ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	//implemented a stack so that if enabled, players will spawn multi-coloured!
	
	
	player->SetPlayerId( inPlayerId );
	//gotta pick a better spawn location than this...
	if (returnPlayerNum() % 2 == 0) { //modulus function used to find if numbers are divisible by 2, true puts in one team, false in another!
		//team 1!
		player->SetLocation(Vector3(-2.5f, 2.f - static_cast<float>(inPlayerId), 0.f));
		player->SetTeamNum(1);
		player->SetColor(Colors::Blue);
		
	}
	else {
		//team 2!
		player->SetLocation(Vector3(3.5f, 2.f - static_cast<float>(inPlayerId), 0.f));
		player->SetTeamNum(2);
		player->SetColor(Colors::Red);
	}

}

void Server::SpawnBullet(int inPlayerWhoShotId)
{
	BulletPtr bullet = std::static_pointer_cast<Bullet>(GameObjectRegistry::sInstance->CreateGameObject('BULT'));
	bullet->SetColor(Colors::Red);
	bullet->SetLocation(Vector3(1.f, 0.f, 0.f));

}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's player
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();
	DeletePlayerNum(); //removes 1 from the player count, so new clients can be distributed
	//ScoreBoardManager::sInstance->RemoveEntry( playerId );
	PlayerPtr player = GetPlayer( playerId );
	if( player )
	{
		player->SetDoesWantToDie( true );

	}
}

PlayerPtr Server::GetPlayer( int inPlayerId )
{
	//run through the objects till we find the Player...
	//it would be nice if we kept a pointer to the Player on the clientproxy
	//but then we'd have to clean it up when the Player died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];

		/* Original code did this in a weird way, used a method in the base (GameObject) which
		returned a player object if a game object is the player and null otherwise */

		uint32_t type = go->GetClassId();

		//Player* player = dynamic_cast<Player*>(*go);
		PlayerPtr player = nullptr;
		if(type == 'PLYR')
		{
			player = std::static_pointer_cast< Player >(go);
		}

		if(player && player->GetPlayerId() == inPlayerId )
		{
			return player;
		}
	}

	return nullptr;

}
