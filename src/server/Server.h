#ifndef SERVER_H
#define SERVER_H

#include "Engine.h"
#include "ClientProxy.h"
#include "Player.h"
#include "Bullet.h"
#include "Block.h"
#include "SpeedyPickup.h"
#include "NetworkManagerServer.h"
#include <stack>

class Server : public Engine
{
public:

	static bool StaticInit();

	virtual void DoFrame() override;

	virtual int Run();

	void HandleNewClient( ClientProxyPtr inClientProxy );
	void HandleLostClient( ClientProxyPtr inClientProxy );
	void AddPlayerNum() { numOfPlayers++; }
	void DeletePlayerNum() { numOfPlayers--; }
	void SpawnSpeedPickup();
	void SpawnPickupPoll();
	void pushColour(Vector3 colour) { PlayerColours.push(colour); }
	Vector3 popColour() { if (!PlayerColours.empty()) { Vector3 returnvalue = PlayerColours.top(); PlayerColours.pop(); return returnvalue; } else { return Vector3(1.0f, 1.0f, 1.0f); } }

	int returnPlayerNum() { return numOfPlayers; }
	PlayerPtr	GetPlayer( int inPlayerId );
	void	SpawnPlayer( int inPlayerId );
	void	SpawnBullet(int inPlayerWhoShotId);


private:
	Server();

	bool	InitNetworkManager();
	void	SetupWorld();
	int		numOfPlayers; //separate into team!
	float mTimeBetweenPickupChecks;
	float mTimeOfNextSpawn;
	SpeedyPickupPtr Pickup;
	std::stack<Vector3> PlayerColours; //using a stack to hold player colour info

};

#endif // SERVER_H
