#include "BulletServer.h"
#include "GameObjectRegistry.h"
//#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"

BulletServer::BulletServer() :
	mBulletControlType(ESCT_AI)
{}

void BulletServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void BulletServer::Update()
{
	Bullet::Update();

	/*
	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();
	float oldRotation = GetRotation();

	ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy(GetBulletId());
	if (client)
	{
		MoveList& moveList = client->GetUnprocessedMoveList();
		for (const Move& unprocessedMove : moveList)
		{
			const InputState& currentState = unprocessedMove.GetInputState();
			float deltaTime = unprocessedMove.GetDeltaTime();
			//SimulateMovement(deltaTime);
		}

		moveList.Clear();
	}
	*/
}